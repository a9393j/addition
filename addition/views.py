from django.shortcuts import render
from addition.forms import *
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse

# Create your views here.

def adding(request):
  if request.method == 'POST':
    form = Output(request.POST)
    if form.is_valid():
      cd = form.cleaned_data
      a = cd ['a']
      b = cd ['b']
      c = a + b
      return render_to_response('addition/output.html',{'form':form, 'input1':a,'input2':b,'output':c},context_instance=RequestContext(request))
  else:
    form = Output()
    return render_to_response('addition/add.html',{'form':form},context_instance=RequestContext(request))
